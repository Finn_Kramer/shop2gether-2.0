# Shop2Gether

[![pipeline status](https://git.mylab.th-luebeck.de/swt-1/shop2gether/badges/main/pipeline.svg)](https://git.mylab.th-luebeck.de/swt-1/shop2gether/-/commits/main)

Shop2Gether ist eine datenbankgestützte App, um Einkaufslisten für den persönlichen Gebrauch sowie für die Verwendung in Gruppen zu verwalten. Für Details konsultieren Sie das Lastenheft (siehe https://bit.ly/shop2gether-lastenheft).

## APK herunterladen

[![Download](https://git.mylab.th-luebeck.de/swt-1/swt-1/-/raw/185826e36cdb094aa9e88432c351628fbd1ab21e/Entwicklung/image/Download_Android_APK.svg)](https://bit.ly/shop2gether-apk)


**Hinweis:** Dabei handelt es sich um die aktuelle APK-Datei, die durch die Pipeline durch den Job _assembleDebug_ erzeugt wurde. Wenn die Pipeline fehlgeschlagen ist, wird von einem Download abgeraten. Der Download startet dierekt nach dem Klick auf das entsprechende Bild.

## Dokumentation

Siehe https://git.mylab.th-luebeck.de/swt-1/swt-1#entwicklung-des-prototypen-abgabe_40
