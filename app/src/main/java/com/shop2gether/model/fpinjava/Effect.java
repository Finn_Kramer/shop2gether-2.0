package com.shop2gether.model.fpinjava;

public interface Effect<T> {
    void apply(T t);
}
