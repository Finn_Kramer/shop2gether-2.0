package com.shop2gether.model;

import java.util.ArrayList;
import java.util.Random;

public class Group extends User {

    private ArrayList<ShoppingList> shoppingLists = new ArrayList<>();

    protected Group(String name, int id) {
        super(name, id);
    }
    
    public static Group create(String name) {
        return new Group(name, new Random().nextInt(Integer.MAX_VALUE));
    }

    public ArrayList<ShoppingList> getShoppingLists() {
        return shoppingLists;
    }

}
