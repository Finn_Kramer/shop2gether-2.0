package com.shop2gether.model;

// Implementiert als unveränderbare Datenstruktur
// Nicht die Item-Klasse unserer Schwesterngruppe
public class Item {

    private final String name;

    protected Item(String name) {
        this.name = name;
    }

    public static Item create(String name) {
        return new Item(name);
    }

    public String getName() {
        return name;
    }

}
