package com.shop2gether.model;

public class Product extends Item {

    private Boolean checkOff = false;
    private final String description;
    private final int points;

    protected Product(String name, String description, int points) {
        super(name);
        this.description = description;
        this.points = points;
    }

    public static Product create(String name) {
        return new Product(name, "Test", 0);
    }

    public String getDescription() {
        return description;
    }

    public int getPoints() {
        return points;
    }

    public boolean toggleTick() {
        this.checkOff = !this.checkOff;
        return true;
    }

    public boolean isCheckOff() {
        return checkOff;
    }

}
