package com.shop2gether.model;

import java.util.ArrayList;

public class ShoppingList extends Item {

    private final ArrayList<Product> products;

    protected ShoppingList(String name, ArrayList<Product> products) {
        super(name);
        this.products = products;
    }

    public static ShoppingList create(String name, ArrayList<Product> products) {
        return new ShoppingList(name, products);
    }

    public static ShoppingList create(String name) {
        return new ShoppingList(name, new ArrayList<>());
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

}
