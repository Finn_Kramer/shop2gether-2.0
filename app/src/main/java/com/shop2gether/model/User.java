package com.shop2gether.model;

import java.util.Random;

public class User extends Item {

    private final int id;

    protected User(String name, int id) {
        super(name);
        this.id = id;
    }

    public static User create(String name) {
        return new User(name, new Random().nextInt(Integer.MAX_VALUE));
    }

    public int getId() {
        return id;
    }

}
