package com.shop2gether.manager;

// key-value storage mit Hawk (siehe https://github.com/orhanobut/hawk)
import static com.orhanobut.hawk.Hawk.*;
import java.util.ArrayList;
import java.util.function.BiPredicate;

import android.content.Context;
import com.shop2gether.model.Group;
import com.shop2gether.model.Product;
import com.shop2gether.model.ShoppingList;
import com.shop2gether.model.fpinjava.Result;

// Mit Singleton Pattern
public class GroupManager {

    private static GroupManager instance;
    private static final String key = "group";
    private ArrayList<Group> groups;

    private GroupManager(Context context) {
        init(context).build();
        this.groups = get(key, new ArrayList<>());
    }

    // synchronized => zum Schutz vor Wettlaufsituationen (Race condition)
    public static synchronized GroupManager getInstance(Context context) {
        if (null == instance) instance = new GroupManager(context.getApplicationContext());
        return instance;
    }

    private boolean save() {
        return put(key, groups);
    }

    // De-morgansches Gesetz: !(a && b) == !a || !b
    private static final BiPredicate<Integer, ArrayList> outOfBounds = (t, l) -> !(0 <= t && t <= l.size());

    public ArrayList<Group> getGroups() {
        return groups;
    }

    public ArrayList<ShoppingList> getShoppingLists(int groupPosition) {
        return outOfBounds.test(groupPosition, groups) ? new ArrayList<>() : groups.get(groupPosition).getShoppingLists();
    }

    // Hier mit Result
    public ArrayList<Product> getProducts(int groupPosition, int listPosition) {
        return Result.of(() -> getShoppingLists(groupPosition).get(listPosition).getProducts()).getOrElse(new ArrayList<>());
    }

    public boolean addGroup(Group group) {
        return groups.add(group) && save();
    }

    public boolean removeGroup(Group group) {
        return groups.remove(group) && save();
    }

    public boolean addShoppingList(int groupPosition, ShoppingList shoppingList) {
        return groupPosition >= 0 && getShoppingLists(groupPosition).add(shoppingList) && save();
    }

    public boolean removeShoppingList(int groupPosition, ShoppingList shoppingList) {
        return getShoppingLists(groupPosition).remove(shoppingList) && save();
    }

    public boolean addProduct(int groupPosition, int listPosition, Product product) {
        return getProducts(groupPosition, listPosition).add(product) && save();
    }

    public boolean removeProduct(int groupPosition, int listPosition, Product product) {
        return getProducts(groupPosition, listPosition).remove(product) && save();
    }

    public boolean toogleTick(int groupPosition, int listPosition, int productPosition) {
        return !outOfBounds.test(productPosition, getProducts(groupPosition, listPosition))
                && getProducts(groupPosition, listPosition).get(productPosition).toggleTick() && save();
    }

    public int getNumberOfGroups() {
        return groups.size();
    }

}
