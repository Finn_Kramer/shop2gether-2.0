package com.shop2gether.controller.listAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.shop2gether.R;
import com.shop2gether.manager.GroupManager;
import com.shop2gether.model.Product;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

// https://developer.android.com/guide/topics/ui/layout/recyclerview
public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {

    int groupPosition, listPosition;
    private GroupManager groupManager;

    private ArrayList<Product> products() {
        return groupManager.getProducts(groupPosition, listPosition);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView title, description;
        private final ImageButton btnFlag, btnCheck;

        public ViewHolder(@NonNull View view) {
            super(view);
            title = view.findViewById(R.id.productName);
            description = view.findViewById(R.id.description);
            btnFlag = view.findViewById(R.id.btnFlag);
            btnCheck = view.findViewById(R.id.btnCheck);
        }

        public TextView getTitle() {
            return title;
        }

        public TextView getDescription() {
            return description;
        }

        public ImageButton getBtnFlag() {
            return btnFlag;
        }

        public ImageButton getBtnCheck() {
            return btnCheck;
        }

    }

    public ProductListAdapter(GroupManager groupManager, int groupPosition, int listPosition) {
        this.groupManager = groupManager;
        this.groupPosition = groupPosition;
        this.listPosition = listPosition;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.product, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Product product = products().get(position);
        ImageButton btn = holder.getBtnCheck();

        holder.getTitle().setText(product.getName());
        btn.setBackground(ContextCompat.getDrawable(
                holder.getBtnCheck().getContext(),
                product.isCheckOff() ? R.drawable.ic_check : R.drawable.ic_shopping_cart)
        );

        holder.getBtnCheck().setOnClickListener(view ->
                checkOff(view.getContext(), btn, product, position)
        );
    }

    @Override
    public int getItemCount() {
        return products() == null ? 0 : products().size();
    }

    public void checkOff(Context context, ImageButton btn, Product product, int productPosition) {
        groupManager.toogleTick(groupPosition, listPosition, productPosition);
        btn.setBackground(ContextCompat.getDrawable(context, product.isCheckOff() ? R.drawable.ic_check : R.drawable.ic_shopping_cart));
    }

}
