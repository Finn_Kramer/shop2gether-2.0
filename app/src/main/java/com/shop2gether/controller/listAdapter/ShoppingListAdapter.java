package com.shop2gether.controller.listAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.shop2gether.R;
import com.shop2gether.controller.activity.ProductActivity;
import com.shop2gether.controller.activity.ShoppingListActivity;
import com.shop2gether.manager.GroupManager;
import com.shop2gether.model.Item;
import com.shop2gether.model.ShoppingList;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

// https://developer.android.com/guide/topics/ui/layout/recyclerview
public class ShoppingListAdapter extends RecyclerView.Adapter<ShoppingListAdapter.ViewHolder> {

    private String groupName;
    private int groupPosition;
    private GroupManager groupManager;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView item;
        private final ImageButton btnDelete;

        public ViewHolder(@NonNull View view) {
            super(view);
            item = view.findViewById(R.id.itemName);
            btnDelete = view.findViewById(R.id.btnDeleteShoppingList);
        }

        public TextView getTextView() {
            return item;
        }

        public ImageButton getBtnDelete() {
            return btnDelete;
        }

    }

    public ShoppingListAdapter(GroupManager groupManager, int groupPosition) {
        this.groupManager = groupManager;
        this.groupPosition = groupPosition;
        this.groupName = groupManager.getGroups().get(groupPosition).getName();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ShoppingList item = groupManager.getShoppingLists(groupPosition).get(position);
        holder.getTextView().setText(item.getName());
        holder.getBtnDelete().setOnClickListener(view -> ShoppingListActivity.onClickDelete(item, groupPosition));
        holder.itemView.setOnClickListener(view -> onClickItem(view.getContext(), item, position));
    }

    @Override
    public int getItemCount() {
        return groupManager.getShoppingLists(groupPosition) == null ? 0 : groupManager.getShoppingLists(groupPosition).size();
    }

    protected void onClickItem(Context context, ShoppingList item, int listPosition) {
        Intent showItem = new Intent(context, ProductActivity.class);
        showItem.putExtra("groupPosition", groupPosition);
        showItem.putExtra("listPosition", listPosition);
        showItem.putExtra("groupName", groupName);
        showItem.putExtra("listName", item.getName());
        context.startActivity(showItem);
    }

}

