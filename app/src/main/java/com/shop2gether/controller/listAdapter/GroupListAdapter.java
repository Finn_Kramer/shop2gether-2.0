package com.shop2gether.controller.listAdapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shop2gether.R;
import com.shop2gether.controller.activity.ShoppingListActivity;
import com.shop2gether.manager.GroupManager;
import com.shop2gether.model.Group;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class GroupListAdapter extends RecyclerView.Adapter<GroupListAdapter.ViewHolder> {

    private GroupManager groups;

    public GroupListAdapter(GroupManager groups) {
        this.groups = groups;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView icon, name;

        public ViewHolder(@NonNull View view) {
            super(view);
            name = view.findViewById(R.id.groupName);
            icon = view.findViewById(R.id.groupTextViewIcon);
        }

        public TextView getTextView() {
            return name;
        }

        public TextView getTextViewIcon() {
            return icon;
        }
    }

    @NonNull
    @Override
    public GroupListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new GroupListAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.group, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Group group = groups.getGroups().get(position);
        holder.getTextView().setText(group.getName());
        holder.getTextViewIcon().setText(group.getName().charAt(0) + "");
        holder.itemView.setOnClickListener(view -> onClickGroup(view.getContext(), group, position));
    }

    @Override
    public int getItemCount() {
        return groups.getNumberOfGroups();
    }

    protected void onClickGroup(Context context, Group group, int position) {
        Intent showItem = new Intent(context, ShoppingListActivity.class);
        showItem.putExtra("groupPosition", position);
        showItem.putExtra("groupName", group.getName());
        context.startActivity(showItem);
    }

}
