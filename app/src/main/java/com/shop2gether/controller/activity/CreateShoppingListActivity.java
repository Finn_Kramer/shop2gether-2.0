package com.shop2gether.controller.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.shop2gether.R;
import com.shop2gether.manager.GroupManager;
import com.shop2gether.model.ShoppingList;

import androidx.annotation.Nullable;

public class CreateShoppingListActivity extends Shop2GetherCompatActivity {

    private int groupPosition;
    private EditText inputShoppingListName;
    private GroupManager groupManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Erstelle Einkaufsliste");
        setContentView(R.layout.activity_create_shoppinglist);
        inputShoppingListName = findViewById(R.id.inputShoppingListName);

        groupPosition = getIntent().getIntExtra(keyForGroupPosition, valueNotFound);
        groupManager = GroupManager.getInstance(this);
    }

    public void addNewShoppingList(View view) {
        String input = inputShoppingListName.getText().toString().trim();
        // todo - Validierung könnte sinnvoll sein / Hier jetzt nur beispielhaft
        if (validListName.test(input)) {
            Toast.makeText(
                    view.getContext(),
                    groupManager.addShoppingList(groupPosition, ShoppingList.create(input))
                            ? "➕ Liste hinzugefügt" : "❌ Liste konnte nicht hinzugefügt werden",
                    Toast.LENGTH_SHORT
            ).show();
            inputShoppingListName.getText().clear();
            super.onBackPressed();
        } else inputShoppingListName.setError("Die Eingabe ist fehlerhaft");
    }

}
