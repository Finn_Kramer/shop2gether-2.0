package com.shop2gether.controller.activity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.shop2gether.R;
import com.shop2gether.controller.listAdapter.GroupListAdapter;
import com.shop2gether.manager.GroupManager;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class GroupActivity extends Shop2GetherCompatActivity {

    private RecyclerView recyclerView;
    private GroupManager groupManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_groups);

        groupManager = GroupManager.getInstance(this);
        recyclerView = findViewById(R.id.groups);
        recyclerView.setAdapter(new GroupListAdapter(groupManager));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    public void createNewGroup(View view) {
        System.out.println("🔧 Erstelle neue Gruppe");
        Intent createGroup = new Intent(this, CreateGroupActivity.class);
        startActivity(createGroup, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
    }

    public void createNewGroup(MenuItem item) {
        createNewGroup(item.getActionView());
    }
}
