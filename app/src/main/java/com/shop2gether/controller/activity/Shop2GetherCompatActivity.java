package com.shop2gether.controller.activity;

import java.util.function.Predicate;

import androidx.appcompat.app.AppCompatActivity;

public class Shop2GetherCompatActivity extends AppCompatActivity {

    protected static final int valueNotFound = -1;
    protected static final String keyForGroupName = "groupName";
    protected static final String keyForListName = "listName";
    protected static final String keyForGroupPosition = "groupPosition";
    protected static final String keyForListPosition = "listPosition";

    protected static final Predicate<String> validGroupName = input -> !"".equals(input) && input.matches("[a-zA-Z ]+");
    protected static final Predicate<String> validProductName = validGroupName;
    protected static final Predicate<String> validListName = validGroupName;

}
