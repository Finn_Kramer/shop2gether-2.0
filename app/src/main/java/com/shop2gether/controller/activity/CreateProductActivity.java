package com.shop2gether.controller.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.shop2gether.R;
import com.shop2gether.manager.GroupManager;
import com.shop2gether.model.Product;

import androidx.annotation.Nullable;

public class CreateProductActivity extends Shop2GetherCompatActivity {

    private int listPosition;
    private int groupPosition;
    private EditText inputItemName;
    private GroupManager groupManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Erstelle Produkt");
        setContentView(R.layout.activity_create_product);
        inputItemName = findViewById(R.id.inputProductName);

        Intent intent = getIntent();
        groupPosition = intent.getIntExtra(keyForGroupPosition, valueNotFound);
        listPosition = intent.getIntExtra(keyForListPosition, valueNotFound);

        groupManager = GroupManager.getInstance(this);
    }

    public void addNewProduct(View view) {
        String input = inputItemName.getText().toString().trim();
        if (validProductName.test(input)) {
            Toast.makeText(
                    view.getContext(),
                    groupManager.addProduct(groupPosition, listPosition, Product.create(input))
                            ? "➕ Produkt hinzugefügt" : "❌ Produkt konnte nicht hinzugefügt werden",
                    Toast.LENGTH_SHORT
            ).show();
            inputItemName.getText().clear();
            super.onBackPressed();
        } else inputItemName.setError("Die Eingabe ist fehlerhaft");
    }

}
