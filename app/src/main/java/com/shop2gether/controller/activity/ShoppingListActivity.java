package com.shop2gether.controller.activity;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.shop2gether.R;
import com.shop2gether.controller.listAdapter.ShoppingListAdapter;
import com.shop2gether.manager.GroupManager;
import com.shop2gether.model.ShoppingList;

import androidx.annotation.Nullable;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ShoppingListActivity extends Shop2GetherCompatActivity {

    private String groupName;
    private int groupPosition;
    private TextView groupIcon;
    private TextView groupText;
    private static RecyclerView recyclerView;
    private static GroupManager groupManager;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_shoppinglists);
        recyclerView = findViewById(R.id.lists);
        groupIcon = findViewById(R.id.groupTextViewIconDashboard);
        groupText = findViewById(R.id.groupTextViewDashboard);

        Intent intent = getIntent();
        groupName = intent.getStringExtra(keyForGroupName);
        groupPosition = intent.getIntExtra(keyForGroupPosition, valueNotFound);
        groupIcon.setText("" + groupName.charAt(0));
        groupText.setText(groupName);

        setTitle(groupName);

        groupManager = GroupManager.getInstance(this);
        recyclerView.setAdapter(new ShoppingListAdapter(groupManager, groupPosition));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    public void createNewList(View view) {
        System.out.println("🔧 Erstelle neues Item");
        Intent createItem = new Intent(this, CreateShoppingListActivity.class);
        createItem.putExtra(keyForGroupPosition, groupPosition);
        startActivity(createItem, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
    }

    @SuppressLint("NotifyDataSetChanged")
    public static void onClickDelete(ShoppingList item, int position) {
        groupManager.removeShoppingList(position, item);
        recyclerView.getAdapter().notifyDataSetChanged();
    }

}
