package com.shop2gether.controller.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.shop2gether.R;
import com.shop2gether.manager.GroupManager;
import com.shop2gether.model.Group;

import androidx.annotation.Nullable;

public class CreateGroupActivity extends Shop2GetherCompatActivity {

        private EditText inputGroupName;
        private GroupManager groupManager;

        @Override
        protected void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setTitle("Erstelle Gruppe");
            setContentView(R.layout.activity_create_groups);
            inputGroupName = findViewById(R.id.inputGroupName);
            groupManager = GroupManager.getInstance(this);
        }

        public void addNewGroup(View view) {
            String input = inputGroupName.getText().toString().trim();
            // todo - Validierung könnte sinnvoll sein / Hier jetzt nur beispielhaft
            if (validGroupName.test(input)) {
                Toast.makeText(
                        view.getContext(),
                        groupManager.addGroup(Group.create(input))
                                ? "➕ Gruppe hinzugefügt" : "❌ Gruppe konnte nicht hinzugefügt werden",
                        Toast.LENGTH_SHORT
                ).show();
                inputGroupName.getText().clear();
                super.onBackPressed();
            } else inputGroupName.setError("Die Eingabe ist fehlerhaft");
        }

}
