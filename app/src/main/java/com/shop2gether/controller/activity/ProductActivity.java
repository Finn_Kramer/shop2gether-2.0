package com.shop2gether.controller.activity;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.shop2gether.controller.listAdapter.ProductListAdapter;
import com.shop2gether.R;
import com.shop2gether.manager.GroupManager;
import com.shop2gether.model.Product;

import java.util.Collections;

public class ProductActivity extends Shop2GetherCompatActivity {

    private int listPosition;
    private int groupPosition;
    private GroupManager groupManager;
    private RecyclerView recyclerView;
    private TextView toolbarName;
    private ProductListAdapter productListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_products);
        recyclerView = findViewById(R.id.products);
        toolbarName = findViewById(R.id.productActivityToolbarName);

        Intent intent = getIntent();
        toolbarName.setText(String.format("%s \u2022 %s",
        intent.getStringExtra(keyForGroupName), intent.getStringExtra(keyForListName)));

        groupPosition = intent.getIntExtra(keyForGroupPosition, valueNotFound);
        listPosition = intent.getIntExtra(keyForListPosition, valueNotFound);

        groupManager = GroupManager.getInstance(this);
        productListAdapter = new ProductListAdapter(groupManager, groupPosition, listPosition);
        recyclerView.setAdapter(productListAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        createItemTouchHelper().attachToRecyclerView(recyclerView);

    }

    public void createNewItem(View view) {
        System.out.println("🔧 Erstelle neues Item");
        Intent createItem = new Intent(this, CreateProductActivity.class);
        createItem.putExtra(keyForGroupPosition, groupPosition);
        createItem.putExtra(keyForListPosition, listPosition);
        startActivity(createItem, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
    }

    public ItemTouchHelper createItemTouchHelper() {
        return new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN | ItemTouchHelper.START | ItemTouchHelper.END, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                int fromPosition = viewHolder.getAdapterPosition();
                int toPosition = target.getAdapterPosition();
                Collections.swap(groupManager.getProducts(groupPosition, listPosition), fromPosition, toPosition);
                recyclerView.getAdapter().notifyItemMoved(fromPosition, toPosition);
                return true;
            }

            @SuppressLint({"NotifyDataSetChanged", "ShowToast"})
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                Product product = groupManager.getProducts(groupPosition, listPosition).get(viewHolder.getAdapterPosition());
                groupManager.removeProduct(groupPosition, listPosition, product);
                recyclerView.getAdapter().notifyDataSetChanged();
                Snackbar.make(recyclerView, product.getName(), Snackbar.LENGTH_LONG).setAction("Undo", view -> {
                    groupManager.addProduct(groupPosition, listPosition, product);
                    recyclerView.getAdapter().notifyDataSetChanged();
                }).show();
            }
        });
    }

}