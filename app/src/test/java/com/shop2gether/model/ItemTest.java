package com.shop2gether.model;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ItemTest {

    @ParameterizedTest
    @ValueSource(strings = {"Item 1", "Item\t2", "Item 5\n", "😃"})
    void itemTest(String name) {
        assertEquals(Item.create(name).getName(), name);
    }

}
